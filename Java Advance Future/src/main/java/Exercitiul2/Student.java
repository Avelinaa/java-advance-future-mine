package Exercitiul2;

public class Student extends Person {
    private String typeOfStudy;
    private int yearOfstudy;
    private int studyPrice;

    public Student(String typeOfStudy, int yearOfstudy, int studyPrice, String name, String adress) {
        super(name, adress);
        this.typeOfStudy = typeOfStudy;
        this.yearOfstudy = yearOfstudy;
        this.studyPrice = studyPrice;
    }

    public String getTypeOfStudy() {
        return typeOfStudy;
    }

    public void setTypeOfStudy(String typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }

    public int getYearOfstudy() {
        return yearOfstudy;
    }

    public void setYearOfstudy(int yearOfstudy) {
        this.yearOfstudy = yearOfstudy;
    }

    public int getStudyPrice() {
        return studyPrice;
    }

    public void setStudyPrice(int studyPrice) {
        this.studyPrice = studyPrice;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studyPrice=" + studyPrice +
                ", name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                '}';
    }
    @Override
    public void f() {
        System.out.println("Sunt student" + name);
    }
}
