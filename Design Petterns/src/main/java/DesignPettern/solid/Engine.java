package DesignPettern.solid;

public class Engine {
    public void Run(){
        System.out.println("Engine is running");
    }
    public void Stop(){
        System.out.println("Engine has stopped");
    }
    public void Speed(int speed){
        System.out.println("Speed is " + speed);
    }
}
