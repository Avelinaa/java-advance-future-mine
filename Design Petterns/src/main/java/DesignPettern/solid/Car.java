package DesignPettern.solid;

public interface Car {
    void Start();
    void accelerate();
    void stop();


}
