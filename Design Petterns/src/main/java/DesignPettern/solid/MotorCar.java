package DesignPettern.solid;

public class MotorCar implements Car{
    private Engine engine;
    public MotorCar(){
        this.engine = new Engine();
    }
    public void Start(){
        System.out.println("Disel car");
        engine.Run();
    }

    public void accelerate() {

    }

    public void accelerate(int kph){
        System.out.println("Gas pedal");
        engine.Speed(kph);
    }
    public void stop(){
        System.out.println("Break pedal");
        engine.Stop();
    }
}
