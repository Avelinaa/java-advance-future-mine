package DesignPettern.AbstractFactory.factorys;

import DesignPettern.AbstractFactory.factorys.pizza.Pizza;
import DesignPettern.AbstractFactory.factorys.pizza.PizzaType;

public interface AbstractFactory {
    Pizza create(PizzaType type, int size);
}
