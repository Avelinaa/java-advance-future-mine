package DesignPettern.AbstractFactory.factorys;

import DesignPettern.AbstractFactory.factorys.pizza.Pizza;
import DesignPettern.AbstractFactory.factorys.pizza.PizzaType;

public class Main {
    public static void main(String[] args) {
        AbstractFactory pizzaFactory = new PizzaFactory();
        Pizza comanda1 = pizzaFactory.create(PizzaType.QUATROSTTAGIONI,30);
        Pizza comanda2 = pizzaFactory.create(PizzaType.MARGHERITA,50);
        Pizza comanda3 = pizzaFactory.create(PizzaType.QUATROFORMAGI,20);
        System.out.println(comanda1);
        System.out.println(comanda2);
        System.out.println(comanda3);
    }
}
