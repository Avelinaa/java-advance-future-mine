package DesignPettern.AbstractFactory.factorys;

import DesignPettern.AbstractFactory.factorys.pizza.*;

public class PizzaFactory implements AbstractFactory {
    @Override
    public Pizza create(PizzaType type, int size) {
        switch (type){
            case MARGHERITA:
                Pizza margherita = new PizzaMargherita(size);
                return margherita;
            case QUATROFORMAGI:
                Pizza quatrofromagi = new PizzaQuatrofromagi(size);
                return quatrofromagi;
            case QUATROSTTAGIONI:
                Pizza quatrosttagioni = new PizzaQuatroSttagioni(size);
                return quatrosttagioni;
            default: return null;
        }

    }
}
