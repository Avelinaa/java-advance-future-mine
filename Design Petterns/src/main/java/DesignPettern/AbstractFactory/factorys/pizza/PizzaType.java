package DesignPettern.AbstractFactory.factorys.pizza;

public enum PizzaType {
    QUATROSTTAGIONI, MARGHERITA, QUATROFORMAGI
}
