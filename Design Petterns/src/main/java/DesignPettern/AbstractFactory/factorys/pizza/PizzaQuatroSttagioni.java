package DesignPettern.AbstractFactory.factorys.pizza;

public class PizzaQuatroSttagioni extends Pizza {
    private int size;
    public PizzaQuatroSttagioni(int size){
        this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.QUATROSTTAGIONI.toString();
    }

    @Override
    public String getIngredients() {
        return "Carnati, sunca";
    }

    @Override
    public int getSize() {
        return size;
    }
}
