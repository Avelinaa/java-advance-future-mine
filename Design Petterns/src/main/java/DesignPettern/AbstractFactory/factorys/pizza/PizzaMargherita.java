package DesignPettern.AbstractFactory.factorys.pizza;

public class PizzaMargherita extends Pizza {
    private int size;

    public PizzaMargherita(int size){
    this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.MARGHERITA.toString();
    }

    @Override
    public String getIngredients() {
        return "Blat, mozzarela, rosii, busuioc";
    }

    @Override
    public int getSize() {
        return size;
    }
}
