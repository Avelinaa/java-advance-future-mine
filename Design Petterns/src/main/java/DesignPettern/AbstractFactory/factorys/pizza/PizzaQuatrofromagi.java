package DesignPettern.AbstractFactory.factorys.pizza;

public class PizzaQuatrofromagi extends Pizza{

    private int size;

    public PizzaQuatrofromagi(int size){
        this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.QUATROFORMAGI.toString();
    }

    @Override
    public String getIngredients() {
        return "Gorgonzola, mozzarela";
    }

    @Override
    public int getSize() {
        return size;
    }
}
