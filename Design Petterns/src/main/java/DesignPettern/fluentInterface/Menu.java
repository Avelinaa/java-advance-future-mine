package DesignPettern.fluentInterface;

import java.util.List;

public interface Menu {
    Menu orderPizza(List<String> orders);
    Menu eatPizza();
    Menu payPizza();
}
