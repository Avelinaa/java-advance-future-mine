package DesignPettern.fluentInterface;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Restaurant restaurant = new Restaurant();
        restaurant.name("Capitol").getMenu().orderPizza(Arrays.asList("QattroSttagioni","QuattroSttagioni")).eatPizza().payPizza();
    }
}
