package DesignPettern.fluentInterface;

public interface Pizza {
    Pizza getName();
    Pizza getIngredients();
    Pizza getCost();
}
